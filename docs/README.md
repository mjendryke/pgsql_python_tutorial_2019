# PostgreSQL and Python Tutorial (3 days)
In this tutorial you will get a brief introduction to PostgreSQL and Python programming. Even without prior knowledge of any programming language you should be able to follow along. Since the group is very small I will make this tutorial as interactive as possible. Most of the time you will be coding by yourself to have as much hands-on experience as possible. Please bring a GIS dataset that you are familiar with to load into the database. I also encourage you to think of a programming problem from your research, which we will discuss and hopefully implement as well.

### Goals
Be able to set up a PostgreSQL database with PostGIS extension to store and manage GIS data. Set up your python programming environment; use command line arguments to read, modify and write data, plot graphs and figures, and interact between the database and python.

### Preparation
* Get to know [PostgreSQL](https://www.postgresql.org/about/)
* Get to know [PostGIS](https://postgis.net/)
* Get to know [Python](http://www.learnpython.org/en/Welcome) by going through the examples in “Learn the Basics”. You can run the code online and do not need to install anything. We will repeat some of the exercises during the tutorial.

### Gitlab
The [repository ](https://gitlab.com/mjendryke/pgsql_python_tutorial_2019) is hosted at Gitlab

### Requirements
* A computer with Linux :+1: or Windows
* Internet connection
* Software
    * [PostgreSQL: Downloads](https://www.postgresql.org/download/)
    * [PostGIS — Installation](https://postgis.net/install/)
    * [Welcome to the QGIS project!](https://qgis.org)
    * [DataGrip: The Cross-Platform IDE for Databases & SQL by JetBrains](https://www.jetbrains.com/datagrip/) (Use the 30 Day trail or verify with your university email account)
    * [Sublime Text - A sophisticated text editor for code, markup and prose](https://www.sublimetext.com/)
    * [Miniconda](https://conda.io/en/latest/miniconda.html), if you perfer a GUI you can use [Anaconda](https://www.anaconda.com/download/)

### Optional
* If you want to learn project and code management
    * A [Gitlab](https://gitlab.com/) account, and
    * [Sublime Merge](https://www.sublimemerge.com/)
* If you do not want to install everything on your computer
    * Get [VirtualBox](https://www.virtualbox.org/)
    * and create a VM with Windows10
* A project or an idea what you would like to program that may help you with your research, bring a dataset that you are familiar with.

Note: We will install the software during the tutorial if you are not able to set it up by yourself.

### 3-day program
##### Day 1:
On the first day we will setup PostgreSQL and get to know the basic commands to interact with the database using `psql`. We install PostGIS and load data into the database using QGIS. Using DataGrip, we learn basic commands like `SELECT`, `INSERT`,`UPDATE`, and `DELETE` and perform spatial queries using functions from PostGIS.
##### Day 2:
On the second day we install Anaconda to create a python environment. We learn the basics of python and write our own small programs. We learn about the `>>>` prompt, installing and importing libraries from conda and conda-forge, commandline arguments, logging, and plotting data using `matplotlib`.
##### Day 3:
On the last day we bring our skills from day 1 and 2 together by writing a python program that can interact with the database using `sqlalchemy` to load data into `pandas` and `geopandas` dataframes. Now that we have a python environment installing `OSGeo` became very simple: we will learn how to import and export data from the database using the `ogr2ogr` commands.
