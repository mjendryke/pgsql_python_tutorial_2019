# Day 1: PostgreSQL

## Download PostgreSQL 11
From [https://www.postgresql.org/](https://www.postgresql.org/) you will be directed to [EnterpriseDB](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads) to download the interactive installer for Windows. Other operating systems have different installers.
1. Download Verion 11.3 for Windows x86-64
2. Run `postgresql-11.3-4-windows-x64.exe`
3. Move through the wizard and adjust the install dir and data dir if needed
4. Open Services and check if postgresql service is running
5. Launch the `StackBuilder`
6. Install PostGIS under Spatial Extensions

## Configuration and tuning
Correctly configuring PostgreSQL and tuning it to perform best for your work is not a straight forward task. Check out this [tutorial](https://wiki.postgresql.org/wiki/Tuning_Your_PostgreSQL_Server) to get you started when you want to improve the performance of your PostgreSQL server.

The main configuration file is called `postgresql.conf` and is located in the data directory.

We will do this **crucial** step at after we played a little bit with PostgreSQL.

## Access to the database
You may run into issues connecting to your database. Check out the `pg_hba.conf` file in the data directory and the [documentation](https://www.postgresql.org/docs/9.1/auth-pg-hba-conf.html) on how to setup the various ways to connect.
Let's try if we can connect using `psql`.

## Online ressources
- This is a cheatsheet of the most [common `psql` commands](http://no.gd/cheatsheet.png)
- This is a document for more basic commands from [clhenrick](https://gist.github.com/clhenrick/ebc8dc779fb6f5ee6a88#installation)
- [PostgreSQL Tutorial](http://www.postgresqltutorial.com/)

## `psql`
Find the documentation for `psql` [here](https://www.postgresql.org/docs/9.2/app-psql.html)

Connect to the database server using
```sql
psql.exe -U USERNAME -d DATABASENAME
```
You should see something like `DATABASENAME=>`.
Let's explore a bit:
- `\l` will list all the databases
- `\dn` will list all the schemas
- `\dt` will list all the tables
- `\dt SCHEMANAME.` to list tables in a specific schema
- `\du` will list all the users
- `\c` to connect to another database

Usually you do not want to work in the public schema. So we have to create one.
```sql
CREATE SCHEMA tutorial;
```
and than switch to it by typing
```sql
SET search_path TO tutorial;
```
We have now create a schema to work in and set the search path to this new schema. If you type `\dt` you will see that there is no table in it.
Lets create a table (relation)
```sql
CREATE TABLE tutorial.test_table (id int,name text);
```
Run `\dt` to see if the table is really there. Now create another one by yourself.

Insert some data into the table
```sql
INSERT INTO tutorial.test_table (id, name) VALUES(1,'Ana');
```
and another one
```sql
INSERT INTO tutorial.test_table (id, name) VALUES(2,'Peter');
```
To see what is in the table you can run
```sql
SELECT * FROM tutorial.test_table;
```
You can also insert multiple at once
```sql
INSERT INTO tutorial.test_table (id, name) VALUES(3,'Eva'),(4,'Bob');
```
Now we select all those records where the name is exactly 3 letters long
```sql
SELECT * FROM tutorial.test_table WHERE length(name)=3;
```
and now the inverse
```sql
SELECT * FROM tutorial.test_table WHERE length(name)!=3;
```
Everyone has a 3 letter name except Peter. Let's fix Peter to Jim.
```sql
UPDATE tutorial.test_table SET name = 'Jim' WHERE id = 2;
```
Eva did not show up to class, let's delete her.
```sql
DELETE FROM tutorial.test_table WHERE name LIKE 'Eva';
```
Check if the changes have been made
```sql
SELECT * FROM tutorial.test_table;
```
A new student joins the class, but What happens if we make a mistake in the insert statement
```sql
INSERT INTO tutorial.test_table (id, name) VALUES('abc','Liz');
```
>ERROR:  invalid input syntax for integer: "abc"

>LINE 1: INSERT INTO tutorial.test_table (id, name) VALUES('abc','Liz...

We cannot insert the wrong datatype into the table. A `string` is not an `int`. It is also very annoying to always set the id for each student.
You can get more information about the datatypes of a table with `\d+ tutorial.test_table`

You can keep on using `psql` and run your commands there. With `\i somescript.sql` you can run scripts and execute your code.

## DataGrip
DataGrip is a GUI to interact with the PostgreSQL database. Open it and make a connection to your local PostgreSQL server.
Download DataGrip from [JetBrains](https://www.jetbrains.com/datagrip/download/#section=windows)

- Follow along during the tutorial
- Check the `*.sql` files in the [Gitlab repository](https://gitlab.com/mjendryke/pgsql_python_tutorial_2019)


### Basic Statements

A simple `SELECT` statement as we saw before in `psql`.
```sql
SELECT
    *
FROM
    tutorial.test_table
;
```
This tables has a very poor design. The id is simply a `int` field that does not auto-increment, the table name has no meaning, and so on and so on. Let's fix some of the issues by design a table.
```sql
CREATE TABLE
    tutorial.student_names (
        id serial NOT NULL,
        name varchar NOT NULL)
;
```
We should use an index to perform `JOIN`s more easily
```sql
CREATE UNIQUE INDEX
    student_names_id_uindex
ON
    tutorial.student_names (id)
;
```
And at a `PRIMARY KEY`
```sql
ALTER TABLE
    tutorial.student_names
ADD CONSTRAINT
    student_names_id_pk
PRIMARY KEY (id)
;
```


Let's rename `test_table`
```sql
ALTER TABLE
    tutorial.test_table
RENAME TO
    student_names
;
```

See the result
```sql
SELECT
    *
FROM
    tutorial.student_names
;
```
Let's get the names from `tutorial.test_table` over to `tutorial.student_names` with this handy statement
```sql
INSERT INTO
    tutorial.student_names(name) (SELECT
                                name
                            FROM
                                tutorial.test_table)
;
```
All there?
```sql
SELECT
    *
FROM
    tutorial.student_names
;
```
Good, let's add some more
```sql
INSERT INTO
    tutorial.student_names (name)
VALUES
    ('Egon'),('Lisa'),('Joe'),('Maria'),('Ry')
;
```
Now we use the dialog in `DataGrip` to create a table `classes`.
```sql
CREATE TABLE
    tutorial.classes ( id serial NOT NULL, name varchar NOT NULL);


CREATE UNIQUE INDEX
    classes_id_uindex
ON
    tutorial.classes (id);


ALTER TABLE
    tutorial.classes
ADD CONSTRAINT
    classes_id_pk
PRIMARY KEY (id)
;
```
Add two classes
```sql
INSERT INTO
    tutorial.classes(name)
VALUES
    ('Python'),('SQL')
;
```
Now, how do we link the classes to the students?
.
.
.
We need an extra column in the `student_names` table.
```sql
ALTER TABLE
    tutorial.student_names
ADD COLUMN
    class_id int
;
```
Assign the first 4 students to class 1, the rest to 2.
```sql
UPDATE
    tutorial.student_names
SET
    class_id = 1
WHERE
    id <= 4
;
```
and
```sql
UPDATE
    tutorial.student_names
SET
    class_id = 2
WHERE
    id > 4
;
```
Finally we can `JOIN` both tables to see the name of the student and the class they are in.
```sql
SELECT
    s.id    AS student_id,
    s.name  AS student_name,
    c.name  AS class
FROM
    tutorial.student_names AS s
JOIN
    tutorial.classes AS c
ON
    (s.class_id = c.id)
;
```
Just for fun, update the `class_id` of student 2 in DataGrip (double click the table name and commit changes.)
Bob is not in class SQL.
If we want to view this table more often, without actually storing it we can create a `VIEW`.
```sql
CREATE OR REPLACE VIEW
    tutorial.students_classes AS (
    SELECT
        s.id AS student_id,
        s.name AS student_name,
        c.name AS class
    FROM
        tutorial.student_names AS s
    JOIN
        tutorial.classes AS c
    ON
        (s.class_id = c.id)
)
;
```
## QGIS
### Spatial Data

There is a [Cheat Sheet for PostGIS](http://www.postgis.us/downloads/postgis21_cheatsheet.pdf).

**Import with QGIS**
- Download the QGIS Standalone Installer Version 3.6 (64 bit) from their [website](https://www.qgis.org/en/site/) and install it.
- Download "Admin 0 – Countries" from [Natural Earth Data 1:110m Cultural Vectors](http://www.naturalearthdata.com/downloads/110m-cultural-vectors/), and
- "Populated Places" (the simple version) from [Natural Earth Data 1:50m Cultural Vectors](http://www.naturalearthdata.com/downloads/50m-cultural-vectors/)
- Open QGIS, and make a connection to your database
- Import shapefiles to `tutorial` schema.
- follow the `Spatial_Statments.sql` file

If you have not renames the tables in the import dialog, do it now
```sql
ALTER TABLE
    tutorial.ne_110m_admin_0_countries
RENAME TO
    countries
;
```
and
```sql
ALTER TABLE
    tutorial.ne_50m_populated_places_simple
RENAME TO
    places
;
```
To improve spatial queries it is always a good idea to create an index on the column that holds the geometry information.
This column is often called `geom`
```sql
CREATE INDEX
    countries_geom_index
ON
    tutorial.countries USING gist(geom)
;
```
and
```sql
CREATE INDEX
    places_geom_index
ON
    tutorial.places USING gist(geom)
;
```
What is the SRID of `countries`
```sql
SELECT
    Find_SRID('tutorial', 'countries', 'geom')
;
```
Get more information about SRID and EPSG codes at [spatialreference.org](https://spatialreference.org/ref/epsg/4326/) or [epsg.io](https://epsg.io/?q=3857)

Let's calculate the population density of Fiji
```sql
SELECT
    id                                                                      AS id,
    admin                                                                   AS name,
    pop_est                                                                 AS population_estimate,
    public.ST_Area(public.ST_Transform(geom, 3857)) / 1000000               AS area_sqkm,
    pop_est / (public.ST_Area(public.ST_Transform(geom, 3857)) / 1000000)   AS population_density
FROM
    tutorial.countries
WHERE
    admin = 'Fiji'
;
```
Create Continents
```sql
SELECT
    row_number() OVER(ORDER BY c.continent) as ID,
    c.continent,
    ST_UNION(geom) AS geom
FROM
    tutorial.countries AS c
GROUP BY
    c.continent
;
```

Select the bounding box of Egypt
```sql
SELECT
    c.id,
    c.admin,
    ST_EXTENT(c.geom) AS geom_bbox
FROM
    tutorial.countries AS c
WHERE
    admin = 'Egypt'
GROUP BY
    c.id
;
```
Select all countries that share a border with Afganistan
```sql
SELECT
    c2.id,
    c2.admin
FROM
    tutorial.countries AS c1, tutorial.countries AS c2
WHERE
    ST_TOUCHES(c1.geom,c2.geom) AND c1.admin = 'Afghanistan'
;
```
Select all places that are within countries that start with `U`
```sql
SELECT
    p.id                    AS id,
    p.name                  AS place_name,
    c.admin                 AS country,
    p.geom                  AS geom
FROM
    tutorial.countries AS c
JOIN
    tutorial.places AS p
ON
    public.ST_INTERSECTS(c.geom,p.geom)
WHERE
     lower(c.admin) LIKE 'u%'
;
```
To try out
- Make queries that use `ST_WITHIN()`, `ST_BUFFER()`, `ST_ASTEXT()`,`ST_ASGEOJSON()`, and three other functions that you want to try out. Check the PostGIS documentation for eg. [ST_AsGeoJSON()](https://postgis.net/docs/ST_AsGeoJSON.html)
- Import your own data
- Create a spatial `VIEW` and make it `MATERIALIZED`
- ...
- We will collect your SQL statements here

## Import and Export data using `ogr2ogr`
This comes later.
